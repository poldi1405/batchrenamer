import re


class Filter():
	filterList = []

	def AddFilter(self, FilterName, FilterRegex):
		for filter in self.filterList:
			if FilterName == filter[0]:
				return
		self.filterList.append([FilterName, FilterRegex])

	def NameAvailable(self, FilterName, FilterText):
		if FilterName == "":
			return False

		for filter in self.filterList:
			if FilterName == filter[0]:
				return False

		if FilterText == "":
			return False

		validRegex = False
		try:
			re.compile(FilterText)
			validRegex = True
		except re.error:
			validRegex = False

		return validRegex

	def ApplyFilter(self, Items, filter):
		matches = []
		for item in Items:
			try:
				re.compile(filter)
				if re.match(item):
					matches.append(item)
			except Exception as e:
				return False
		return matches
