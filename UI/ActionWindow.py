# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window_resources/ui_files/ActionWindow.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 290)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 621, 85))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.gridLayout.addWidget(self.lineEdit_3, 2, 1, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 1)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.gridLayout.addWidget(self.lineEdit_2, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 3, 1)
        self.AddActionButton = QtWidgets.QPushButton(self.layoutWidget)
        self.AddActionButton.setObjectName("AddActionButton")
        self.gridLayout_2.addWidget(self.AddActionButton, 0, 1, 1, 1)
        self.comboBox = QtWidgets.QComboBox(self.layoutWidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.gridLayout_2.addWidget(self.comboBox, 1, 1, 1, 1)
        self.AbortButton = QtWidgets.QPushButton(self.layoutWidget)
        self.AbortButton.setObjectName("AbortButton")
        self.gridLayout_2.addWidget(self.AbortButton, 2, 1, 1, 1)
        self.tableView = QtWidgets.QTableView(Dialog)
        self.tableView.setGeometry(QtCore.QRect(10, 120, 621, 161))
        self.tableView.setObjectName("tableView")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(10, 100, 59, 15))
        self.label_4.setObjectName("label_4")

        self.retranslateUi(Dialog)
        self.AbortButton.clicked.connect(Dialog.close)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Add Action"))
        self.label_2.setText(_translate("Dialog", "Where"))
        self.label.setText(_translate("Dialog", "What"))
        self.label_3.setText(_translate("Dialog", "With what"))
        self.AddActionButton.setText(_translate("Dialog", "Add Action"))
        self.comboBox.setCurrentText(_translate("Dialog", "RegEx"))
        self.comboBox.setItemText(0, _translate("Dialog", "RegEx"))
        self.comboBox.setItemText(1, _translate("Dialog", "Replacement"))
        self.comboBox.setItemText(2, _translate("Dialog", "Insertion"))
        self.AbortButton.setText(_translate("Dialog", "Abort"))
        self.label_4.setText(_translate("Dialog", "Preview"))


