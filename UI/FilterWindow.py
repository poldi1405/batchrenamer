# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window_resources/ui_files/FilterWindow.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 258)
        self.toolBox = QtWidgets.QToolBox(Dialog)
        self.toolBox.setGeometry(QtCore.QRect(0, 70, 641, 181))
        self.toolBox.setObjectName("toolBox")
        self.FilterMatchingBox = QtWidgets.QWidget()
        self.FilterMatchingBox.setGeometry(QtCore.QRect(0, 0, 641, 123))
        self.FilterMatchingBox.setObjectName("FilterMatchingBox")
        self.FilterListPreview = QtWidgets.QListWidget(self.FilterMatchingBox)
        self.FilterListPreview.setEnabled(False)
        self.FilterListPreview.setGeometry(QtCore.QRect(10, 0, 621, 121))
        self.FilterListPreview.setObjectName("FilterListPreview")
        self.toolBox.addItem(self.FilterMatchingBox, "")
        self.FilterHelpBox = QtWidgets.QWidget()
        self.FilterHelpBox.setGeometry(QtCore.QRect(0, 0, 98, 28))
        self.FilterHelpBox.setObjectName("FilterHelpBox")
        self.label = QtWidgets.QLabel(self.FilterHelpBox)
        self.label.setGeometry(QtCore.QRect(10, 0, 621, 111))
        self.label.setObjectName("label")
        self.toolBox.addItem(self.FilterHelpBox, "")
        self.gridLayoutWidget = QtWidgets.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 621, 54))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.FilterContent = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.FilterContent.setObjectName("FilterContent")
        self.gridLayout.addWidget(self.FilterContent, 1, 1, 1, 1)
        self.FilterName = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.FilterName.setObjectName("FilterName")
        self.gridLayout.addWidget(self.FilterName, 0, 1, 1, 1)
        self.FilterTextLabel = QtWidgets.QLabel(self.gridLayoutWidget)
        self.FilterTextLabel.setObjectName("FilterTextLabel")
        self.gridLayout.addWidget(self.FilterTextLabel, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.FilterAddButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.FilterAddButton.setEnabled(False)
        self.FilterAddButton.setObjectName("FilterAddButton")
        self.gridLayout.addWidget(self.FilterAddButton, 0, 2, 1, 1)
        self.AbortButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.AbortButton.setObjectName("AbortButton")
        self.gridLayout.addWidget(self.AbortButton, 1, 2, 1, 1)
        self.FilterTextLabel.setBuddy(self.FilterName)

        self.retranslateUi(Dialog)
        self.toolBox.setCurrentIndex(0)
        self.AbortButton.clicked.connect(Dialog.close)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Add Filter"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.FilterMatchingBox), _translate("Dialog", "Matching Items"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p>* Can stand for any amount of characters<br/>? Can stand for one or zero characters<br/>{n} Can stand for n times<br/>[abc]q matches only quantities of specified characters<br/>[^abc]q matches everything but the specified characters</p><p>For more information <a href=\"https://en.wikipedia.org/wiki/Regular_expression\"><span style=\" text-decoration: underline; color:#0000ff;\">click here</span></a></p></body></html>"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.FilterHelpBox), _translate("Dialog", "Filter Help (RegEx)"))
        self.FilterTextLabel.setText(_translate("Dialog", "Name"))
        self.label_2.setText(_translate("Dialog", "Filter"))
        self.FilterAddButton.setText(_translate("Dialog", "Add Filter"))
        self.AbortButton.setText(_translate("Dialog", "Abort"))


