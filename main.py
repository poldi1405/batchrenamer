import os
import sys, _thread

from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QTableView, QFileDialog, QTableWidgetItem, QHeaderView
from PyQt5.QtCore import Qt, QMimeData, QPersistentModelIndex
from PyQt5.QtGui import QDrag, QStandardItemModel
from UI import ActionWindow, MainWindow, FilterWindow
from Renamer import Filter, Items, Actions


class MainGUI(QMainWindow, MainWindow.Ui_MainWindow):
	renameFilter = Filter
	files = Items.Items
	removeLock = False

	def __init__(self):
		print("Initialising GUI")
		super(self.__class__, self).__init__()
		print("Setting up GUI")
		self.setupUi(self)
		# self.fileListing.setModel(QStandardItemModel().setHorizontalHeaderLabels(['Path', 'Destination', 'Result']))
		self.fileListing.setColumnCount(3)
		self.fileListing.setHorizontalHeaderItem(0, QTableWidgetItem("Source"))
		self.fileListing.setHorizontalHeaderItem(1, QTableWidgetItem("Destination"))
		self.fileListing.setHorizontalHeaderItem(2, QTableWidgetItem("Status"))
		header = self.fileListing.horizontalHeader()
		header.setSectionResizeMode(0, QHeaderView.Stretch)
		header.setSectionResizeMode(1, QHeaderView.Stretch)
		header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
		print("Connecting GUI Actions")
		self.AddActionButton.clicked.connect(lambda: self.ShowAddAction())
		self.AddFilterButton.clicked.connect(lambda: self.ShowAddFilter())
		self.AddFileButton.clicked.connect(lambda: self.AddFiles())
		self.AddFolderButton.clicked.connect(lambda: _thread.start_new_thread(self.AddFolders()))
		self.RemoveEntryButton.clicked.connect(lambda: self.RemoveRow())
		print("Displaying GUI")
		self.show()

	# Show file selection dialog
	def AddFiles(self):
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		fileName, _ = QFileDialog.getOpenFileNames(self, "Select files to add", "",
		                                           "All Files (*)", options=options)
		if fileName:
			for file in fileName:
				self.files.AddItem(self.files, file)
			self.UpdateFileList()

	# get filepaths
	def getListOfFiles(self, dirName):
		# create a list of file and sub directories
		# names in the given directory
		listOfFile = os.listdir(dirName)
		allFiles = list()
		# Iterate over all the entries
		for entry in listOfFile:
			# Create full path
			fullPath = os.path.join(dirName, entry)
			# If entry is a directory then get the list of files in this directory
			if os.path.isdir(fullPath):
				try:
					allFiles = allFiles + self.getListOfFiles(fullPath)
				except:
					continue
			else:
				allFiles.append(fullPath)

		return allFiles

	#
	def AddFolders(self):
		directory = str(QFileDialog.getExistingDirectory())

		if directory:
			for files in self.getListOfFiles(directory):
				print(files)
				self.files.AddItem(self.files, files)
			self.UpdateFileList()

	# Remove rows from table
	def RemoveRow(self):
		if self.removeLock:
			return
		self.removeLock = True
		rows = sorted(set(index.row() for index in self.fileListing.selectedIndexes()))
		try:
			self.files.RemoveItems(self.files, rows)
		except:
			print("Error while deleting")
		self.UpdateFileList()
		self.removeLock = False

	# Refresh Table
	def UpdateFileList(self):
		self.fileListing.setRowCount(len(self.files.itemList))
		self.RemoveEntryButton.setEnabled(len(self.files.itemList) != 0)
		i = 0
		for file in self.files.itemList:
			print(file)
			self.fileListing.setItem(i, 0, QTableWidgetItem(file))
			self.fileListing.setItem(i, 1, QTableWidgetItem(file))
			self.fileListing.setItem(i, 2, QTableWidgetItem("OK"))
			i += 1

	# Display Action Add Dialog
	def ShowAddAction(self):
		print("Add Action Dialog")
		print("    Initialising Dialog")
		dialog = QDialog()
		dialog.ui = ActionWindow.Ui_Dialog()
		print("    Setting up Dialog")
		dialog.ui.setupUi(dialog)
		print("    Connecting GUI Actions")
		dialog.exec_()
		dialog.show()

	# Display Filter Add Dialog
	def ShowAddFilter(self):
		print("Add Action Dialog")
		print("    Initialising Dialog")
		dialog = QDialog()
		dialog.ui = FilterWindow.Ui_Dialog()
		print("    Setting up Dialog")
		dialog.ui.setupUi(dialog)
		print("    Connecting GUI Actions")
		dialog.ui.FilterName.textChanged.connect(
			lambda: self.FilterButtonDisable(dialog, dialog.ui.FilterName.text(), dialog.ui.FilterContent.text()))
		dialog.ui.FilterContent.textChanged.connect(
			lambda: self.FilterButtonDisable(dialog, dialog.ui.FilterName.text(), dialog.ui.FilterContent.text()))
		dialog.ui.FilterContent.textChanged.connect(
			lambda: self.FilterUpdatePreview(dialog, dialog.ui.FilterContent.text()))
		print("    Displaying Dialog")
		dialog.exec_()

	# Disable Button for adding Filter
	def FilterButtonDisable(self, dialog, FilterName, FilterText):
		dialog.ui.FilterAddButton.setEnabled(self.renameFilter.NameAvailable(FilterName, FilterText))

	# Update Preview of Filter Dialog
	def FilterUpdatePreview(self, dialog, FilterText):
		dialog.ui.FilterListPreview.clear()
		dialog.ui.FilterListPreview.addItems(self.renameFilter.ApplyFilter(self.files.itemList, FilterText))


if __name__ == "__main__":
	app = QApplication(sys.argv)
	window = MainGUI()
	sys.exit(app.exec_())
