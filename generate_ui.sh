#!/usr/bin/env bash

pyuic5 -o UI/MainWindow.py window_resources/ui_files/MainWindow.ui
pyuic5 -o UI/FilterWindow.py window_resources/ui_files/FilterWindow.ui
pyuic5 -o UI/ActionWindow.py window_resources/ui_files/ActionWindow.ui
